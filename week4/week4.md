# Electronics Production

## **Week objectives:**

1. Fabricate (PCB): **FabISP**.

2. Soldering FabISP components

3. Programming FabISP.

## **Steps of Fabrications PCB**:

1. Design Circuit.

2. Schematic.

3. Layout.

4. PCB Images / Design files --> fabmodules.

5. Milling.

___

For this week's task, we will Fabricate PCB FabISP.Afterward, I'm going to use it in my Fab academy Journey whenever I need to program any board or chip. Moreover, we took this chance to learn the steps of the electronic production of PCB.

There are many _techniques_ can be used to Fabricate PCBs:

The **first technique** I did try this week was [Milling](https://www.lexico.com/en/definition/milling) which is a machine cut or shape materials using a rotating tool.

The PCB images were already (traces and outline), these images shows exactly carves that will be in the bored. What I need to do is open [fab modules](http://fabmodules.org/) to convert PCB images into formate understandable by the machine that includes the movements of the machine need to do it to produce the bore


In the lab we are having ```Roland SRM-02```CNC Machine its working based on milling technique , and [CNC](https://www.cnccookbook.com/what-is-cnc-machining-and-cnc-machines/) stand for Computer Numerical Control the picture below shows the machine:

![](images4/monofab.jpg)

Fab modules create ```.rml``` file because we did use a Roland machine that includes the movements of the machine need to do it to produce the bored.

The images below shows the input settings. I need to select for both traces and outline, they almost the same the only difference in step-1 for traces images I select PCB traces with (1/64 inch) size of the milling bit. On the other hand, for outline I select PCB outline (1/32 inch), the milling bit size effect how I want the bored to cut. For traces I need to graver, but for the outline in need to be snip the FR1 plate to separate the PCB.

For step-2 I selected the outputs, first the machine available in LAB is SRM-20, step-3 selecting the reference points for X , Y ,and, Z .

![](images4/first.png)

![](images4/sec.png)

Finally, the last step-4 press calculate this will generate Roland mill file, then save it to use after in the software, the figures below shows the paths the milling bit will make.

![](images4/imgF.png)

for the _material_ I did use what available in the lab which is **FR1** and its safe compering to other materials in market.

## PCB Milling Steps:

1. Fixing FR1 in the machine.

As we will use the same FR1 sheet to save materials & avoiding any waste, so we did The set up together.

![](images4/img505.png)

___

![](images4/img5.png)

The above pictures show the steps of milling the traces. First, I did use milling bit size 1/64 inch ,and, I use V-panel for SRM-20 to set the origin point for X Y Z ,whenever I’m done I press cut and add trace file.

![](images4/img666.png)

![](images4/img7.png)

For the outline is the same steps but I changes the milling bit to 1/32 inch, and I only set Z origin without changing X and Y , Finally I took out the PCB out as shown above figures.

![](images4/img8.png)

**Finally My _PCB_  is Ready ^^ , Now I can start Soldering :)**

## Organizing the components:

![](images4/img9.png)

## Soldering FabISP components:

I did start by organize the tools in my solder station as shown in picture below, I always enjoy soldering especially SMD components, because it lets me feel relaxing and I love to see the board full of components in the END.

#### Needed Tools:

1. Tweezers.
2. Iron and Solder.
3. Components.
4. Schematic & Layout . --> To Know where to place the components.

![](images4/img100.png)

**My PCB after soldering:**

![](images4/img11.png)

## Cable wiring for _FTDI connector cable_ :

FTDI cable I will need to use it in case I want program any PCB throught my FabISP.

**Equipment :**

![](images4/FTDI.png)

I did insert the 2x3 connector from both sides of ribbon cable, then I used the hammer to lock them as shown below in picture.

![](images4/FTDI2.png)

## Programing FabISP with another ready ISP programmer:

### VERY IMPORTANT :

To insure there is no short circuit before plugging the FabISP in laptop otherwise it will burn, I did continuity test using multimeter as shown below in figure.

![](images4/img20.png)

* First before programming there is some softwares I need to download them in my Mac **as mention in steps below**:

1. Install [crossPack](https://brewinstall.org).

2. I did follow this [tutorial](http://maxembedded.com/2015/06/
  setting-up-avr-gcc-toolchain-on-linux-and-mac-os-x/) to setting up AVR-GCC toolchain on my Mac.

>Note : Many thanks to my colleague [Shaikha](http://fabacademy.org/2020/labs/uae/students/shaikha-almazaina/) also a mac user finds out the commands we need to type, as we did face some problems while installing because of the new update of Mac ```version 10.15.2``` macOS Catalina not compatible with corssPack version.)

Install Homebrew

- **open terminal bash mode**.

Type:

1. ```brew tap osx-cross/avr```

2. ``` ruby -e "$(curl -fsSL https://raw.githubusercontent.com/
  Homebrew/install/master/install)" < /dev/null 2> /dev/nul```

3. ```brew install binutils```

4. ```brew install gcc```

5. ```brew tap osx-cross/avr && brew install avr-gcc```

6. ```ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" < /dev/null 2> /dev/null```

7. ```Brew install avrdude```

**To insure the right version install in the laptop type :**

8. ```avr-gcc --version```

![](images4/v.png)

**Restart terminal then type:**

This command insure that ```make``` command is working  ```Make -v```

![](images4/make.png)

3. Firmware source code

Extract the zip file --> open terminal cd to directory I want save in file --> Run ```make```.

when running is done, a file called ```fts_firmware.hex.```created see the picture below:


![](images4/img13.png)

![](images4/img112.png)

## Steps to active the programmer :

1. Run ```make flash```

Now after finishing install all needed softwares it is the time to program my FabISP there is 3 commands to type on terminal, first I need to ```cd``` in directory I have the ```fta_firmware.hex ```file and do the connection shown in picture below, Moreover I did use a Hub because the USB bus in my device is USB 3 and FabISP working only with USB 2 :

![](images4/img14.png)

2. Run ```make fuses```

Now, I need insure that my Mac recognize my FabISP from system report as shown in image below

![](images4/img15.png)

3. Run ```make rstdisbl```

It will blow the rest fuse. After Doing this step I will not be able to program my FabISP with another ISP programmer once the rest fuse is burned.


**Now I have ready programmer FabISP , Finally I did it!!! XD**

## Faced Problems:

I faced a lot of problems to program my board , First I did burn the ATniy45 when I plug the FTDI cable with wrong direction, so I went to de-solder it but I did use ```Wick``` and this was my biggest mistake because it took from me a lot of time, and, I end up cutting some of the tracks and pads in my PCB.

![](images4/img656.png)

For the second time, I did also burn it because I did reverse the connection between the ready programmed ISP and my ISP.

For this time to De-soldering, I did use a ```heat gun```, but Unfortunately, I aimed to desolder only the damaged ATniy45 but I did de-solder the near components.

![](images4/img77.png)

I think I did this mistake because I did the set the heat very high and I didn't direct the gun in the correct place.


However, I didn't lose the hope of using the same PCB, I did use wires left from used ribbon cable we have in lab to do the needed connections see the picture below showing my first PCB with all modification I did to let it work.

![](images4/img16.png)

Still, whenever I do solder there was a Short Circuit in my PCB and Unfortunately, I can't use it anymore ```:(``` it took for me so many hours to fix it, I was hoping using it even it was looking ugly because I did make effort to make it work.
