
# Week 3 : Electronics Design

### **Week objectives:**
1. Redraw **"Eco Hello world board"** by adding  at least one input and output using " Autodesk Eagle" software.

3. Programming ATtiny44.

2. Using another software to do the Schematic and Layout.

### Used software:

- Autodesk Eagle. (PCB Desgin) ![](images3/Eagle.png)

- Preview. (Edit photo) ![](images3/prق.png)

- Arduino. (Programming) ![](images3/ard.jpg)


- DipTrace. (PCB Desgin) ![](images3/dip.png)


### PCB Desgin steps:
- Schematic.
- Layout.
- Milling.
- Soldering.

### Schematic Diagram:

It's really important to start the PCB Desgin with schematic rather then jump street way to the layout, having nice and net schematic will let my work more easier in doing the tracks and traces, also its important to have idea how i want PCB design to look like therefore we must have the [data sheet](https://pdf1.alldatasheet.com/datasheet-pdf/view/313145/ATMEL/ATtiny44.html) of the microcontroller we gone to use which is Atniy 44 from the pin out we will know where to connect the extra input and output.

To Design my schematic I did use Autodesk Eagle, This software was new for me, I used to use Free software called DipTrace To PCB design and in fact I like it DipTrace more then in Eagle, In my opinion it was more easier to use and no need for WiFi.

First i did start with adding [Fab library](https://gitlab.cba.mit.edu/pub/libraries) that includes all the components I will need and available in the LAB the steps of adding library in Eagle is mention below:

1. open Eagle folder, and Download fab file that have .lbr so we can add it after to libraries and save it.
![](images3/img21.png)

2. Afterward, Open Documents you will find Eagle folder generator automatically after Downloading eagle, Inside it there is folder called libraries open it and then move the (( eagle_fab.lbr )) to the libraries folder, Its important to move not copying it otherwise it will not appear in eagle.
![](images3/img2222.png)


3. Know I can use Fab library whenever i want.

To start I create a new project then select new > schematic as shown in picture below

![](images3/img7.png)

The Image below show my final schematic, i did use the given PCB design for [echo hello world](http://academy.cba.mit.edu/classes/embedded_programming/index.html#echo) as reference:

![](images3/img8.png)

Used Components :

1. ATtiny44.
2. Resistor "R1=10K ohms".
3. Capacetior "C1= 1 MicroF".
4. XTAL Resonator 20 MHz.
5. FTDI connector (1x6 header).
6. ISP connector (2x3 header).

I did add :

1. botton.
2. Green LED with Resistor "R2= 499 ohms"

All the controls in eagle happen using " Command Toolbar" the image below shows the functions of the necessary symbols:

![](images3/img9.png)

After finishing all the placements and connections of the components and checking everything is connected in right place, i did convert Schematic file to Layout file as shown below:

![](images3/img10.png)

### layout :

The picture below shows the generator board with the connections we need to route it in yellow color, as shown the components generator in random areas, I need to choose where to place the components in the bored then route them,
![](images3/img12.png)
also its important to check Desgin rule check (DRC) before routing

The picture below shows my final layout :

![](images3/lay.png)

The images below shown the steps of exporting layout to images, to able to use it in fabmodules Afterward.

![](images3/img16.png)

After the image was generated I check the resolution, and it was scaled to double the size , so i did scale it down by 50 % to the same size generated in eagle using Preview as shown below, otherwise, if I convert the image in fabmodules with the wrong scaling I will face problems in milling, the machine will not print compatible size.

![](images3/img17.png)

### Fabmodules files:

![](images3/img18.png)

### This is my final PCB board after soldering:

![](images3/img19.png)


## Files:
- Schematic file.
- layout file.
