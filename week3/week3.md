# Computer Controlled Cutting

## **Week objectives:**

### - Group Task:

 - Characterizing laser cutter's focus, power, speed, rate, kerf and joint clearance.

###  - individual Task:

- Cut/Print sticker using Vinyl Cutter machine.

- Design a parametric press fit construction kit and cut it using laser cutting machine.

<!--which can be assembled in multiple ways. Account for the laser cutter kerf.

linked to the group assignment page Explained how you parametrically designed your files
Documented how you made your press-fit kit
Documented how you made your vinyl cutting
Included your original design files
Included your hero shots

-->


### used software:

1. corelDRAW.

2. Roland CutStudio

3. Autodesk Fusion.

4. Universal Laser System interface.

In this week the aim was to be familiar with all cutting machines available in the lab to be able to implement 2D and 3D designs.

### Print sticker:

 First I did start by using **Roland vinyl Cutter model CAMM-1 GS-24**, I did choose to print the new [UAE Nation Brand Logo](https://www.nationbrand.ae/ar/brand).

 ![](images4/uae.png)

Before start printing and upload the logo in the Computer I did adjust the machine by putting the sticker roll. I did use black color only, then the cutter will calculate the width of the sheet that i will need to use it after in the software.

![](images4/img1.png)

Moreover, I did test the force, I have find out the force give me the best cutting for the black sheet is 30gf, the testing was done by keep pressing test button in the cutter and it will cut for me a sample to see if used force good or not as shown in images below.

![](images4/img2.png)

Afterward, I did open corelDRAW and follow the mention steps below:

1. Create new file.

2. Upload the logo in corelDRAW.

3. Right click in logo and choose " Quick Trace" this separates the image and create vector that I need for printing.

4. Left click **"no color box"**, then Right click **"Red color boxx"** to display the traces .

![](images4/img3.png)

5. Press in the tool bar "Roland CutStudio" and select the traces want to cut and do update for the preview.

6. Press in the preview "Roland CutStudio" to send it the machine software.

![](images4/img4.png)

7. In Roland sowftware, select the logo and choose where to print in the sheet.

8. Then for adjusting size, I click **file** > **cutting setup** > **size change** > for the **width** I enter the width calculated by the machine in my case was 584 m, for the length depend on the design so I see the length of the logo in corelDRAW  i put it 100 m.

![](images4/img6.png)

9. Finally, press **cutting** the machine will start printing.

![](images4/img7.png)

Know is the time to prepare the sticker as shown in the images below:

![](images4/img9.png)

**The final look of the sticker shown below:**

![](images4/img10.png)

Application Tape


### Group Task

For the group assignment each student selected a material to identify there features for different laser cutter in the Lab there is three machines which are :

1. Universal Laser System ULS (PSL6MW).

2. Universal Laser System (ILS12.150D).

3. trotec laser machine.

We did share the setting of machines in Google Docs so we will be able to use this settings whenever we need the machines as shown below the table :

![](images4/table.png)

I did select: **Plastic (Cast Acrylic 3mm) color blue**, Of course I did use board already used to preserve materials.

![](images4/img21.png)

There are two things we need to do to figure out the perfect cutting settings:


#### 1. For power, speed ,and, rate: cutting and engrave any shape.

I have draw square(test cutting) and write my name inside it(test engraving) in corelDRAW.
Moreover, ULS (PSL6MW) understand/used **Red** color for cutting and black for engraving, so the I have selected the square outline red color to cut it, also I must select the line   type  to (hairline). In the other hand,  for engraving I have select the name with **black** color.

![](images4/img23.png)

Next, click **File** >> **Print** >> **Preferences** >>  Universal Laser systems control panel page will pop to add the **setting** needed for the machine.

Its important to add the right thickness of material otherwise we may having errors during cutting so we measure the the thickness using Digital Caliper from all the sides and then take the average, I found it equal to **3.152 mm**.

Although, the material thickness is know from the manufacturer but still we need to measure it to avoid any possible error.

![](images4/img23.png)

![](images4/img24.png)

As shown above first for Material Database I select **plastic** >> **Cast Acrylic**, also I entered the material thickness **3.152 mm** >> then press **Apply**.

Second, _Manual control **black** color for engraving:_

- Power : 100%

- Speed:  100%

- PPI:    500

_Manual control **Red** color for Cutting:_

- Power : 50%

- Speed:  3.8%

- PPI:    300


Perfect settings for (cutting and engrave) for Cast Actylic :

 _Manual control **black** color for engraving:_

- Power : 26%

- Speed:  100%

- PPI:    500

_Manual control **Red** color for Cutting:_

- Power : 100%

- Speed:  3.9%

- PPI:    1000

![](images4/img24.png)

The above setting result the cutting and engraving shown below in the image it clear that the laser did not cut the board for the first attempt(failed), but the engrave was perfect so i did try one more time and I changed the material thickness to **3 mm** and use the default setting for "Cast Actylic" and every thing went prefect for the cutting and engraving ,i have recode this settings in the table for future use.

The image below shown the laser cutting for the mention settings above :

![](images4/img27.png)

After entering all value I press **"set"**, then I open Universal Laser System control panel from windows toolbar.

![](images4/img25.png)

The focus in this machine is automatic we choose the **"focus view"** icon, then we move the design to the place we want it to be printed by using **"Relocated View"** then press **" To Pointer"** to put the design in place I choose then press play button to start **cutting**.

![](images4/img26.png)

#### 2. Calculate the kref.

[Kref](https://www.esabna.com/us/en/education/blog/what-is-cutting-kerf.cfm) is the width of material that is removed by a cutting process.
For the kref calculation, one of the student create rectangular (60x25 mm) and section it to 12 equal parts we can have more sections, increasing the sections number will make the calculation more accurate, all of us used the same file to insure having uniform dimensions. Afterward, we used Digital Caliper to measure the length after cutting in order to find the kref.

- I found the  **kref of Cast Actylic = 0.331 mm** using the equaiton below

![](images4/img28.png)

### Press fit Kit:

In begin I decided to build " house " consist of cube and pyramid. However, it took from me a lot of time. Thus I decided to try it at another time to be able finish the assignment for this week, also to not waste more material.

My press fi kit design consist of outline circle inside it hexagon shape with 3 slots, and 3 columns and connectors using Fusion. The behind idea my structure to display the importance and element of team work.

I did set the dimension parameters in order to have a parametric design as shown below:

![](images4/img11.png)

![](images4/imgp.png)

Then press **Finish sketch** to save the file in formate understandable by corelDRAW which I need to use it after with the Universal Laser System. After that, **Right click on sketch** save as DXF.

![](images4/img14.png)

Then open the file corelDRAW in computer connected to the laser cutter.I did some edits before printing I did write words in the columns engrave it.

![](images4/img12.png)

![](images4/img133.png)

- Used material in this task was "Cardboard" which not expensive and easy to find.

- Used machine was **"Universal Laser systems PLS6MW**, I did used the cutting setting obtained in the group task for more details see my colleague [Maha](https://maha_alhashimi.gitlab.io/mahawebsite/week3/computer-controlled-cutting.html) documentation since she did the setting for this material.

_Manual control **black** color for engraving:_

- Power : 90%

- Speed:  10%

- PPI:    500

_Manual control **Red** color for Cutting:_

- Power : 80%

- Speed:  7.0%

- PPI:    300

After printing and starting to set up my Kit I figure out that I need connectors to have the third layer

![](images4/img18.png)

so I did some modifications in the column and add slot to fit the connectors in them.

The image below shows my **final press fit kit** from different sides View:

![](images4/img88.png)

![](images4/img99.png)



Files:

- Fusion.

- corelDRAW.
