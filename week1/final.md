# Final project idea

## Time Control Machine Glove
### 1. _Sketch_:
<br />

![](images/HH.png)
<br />
<br />

### 2. _The Function of the project and its uses_ :
<br />

* The aim of this project is to stop the movement of any subject such as Fan, drill, and, Alarm clock, etc..
<br />

* The principle of working for the Time Machine its function as strobe controlled by Arduino, by controlling the frequency of flashes through accelerometer and potentiometer, therefore when the frequency of strobe synchronize with the frequency of the movement subject it will stop.



[Reference](https://www.instructables.com/id/DIY-Time-Control-Machine/)
