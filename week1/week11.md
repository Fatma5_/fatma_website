# Project Management.

## Week objectives:

1. To be familiar with using GitLab.

2. Build our own website and learn some HTML.

## New software used this week:

1. Git.

2. Brackets.

3. Atom.

4. Pandoc.

### To start with Gitlab:

First I did download [Git](https://git-scm.com/) for Mac in my laptop. Git is VCS (version control system) to locally track changes in my projects/folders and push & pall changes from remote repositories like Gitlab. this definition was mention in this [tutorial](https://www.youtube.com/playlist?list=PLhW3qG5bs-L8YSnCiyQ-jD8XfHC2W1NL_) I watch it learn more about  Git and GitLab.

![](images/screen1.png)

The second step is Installing Git, I faced some problems when I try click the package of compressed installer files to install it, I figure out then I need to press right click and click on open to give permeation to download the file. This happen sometime in Mac when free programs are downloaded from internet for safety reasons.

![](images/img1.png)

The next step is to create account in (Gitlab), its powerful tool to manage projects and sharing them. I hope I know about it before, I think it will be useful to it use it in university.

![](images/img2.png)

### Configuring Git

The first step in configuration Git in Mac is to open the Terminal and type ```git --version``` to check if Git correctly installed by showing the number of the version. we need to do the configuration only once to connect my device with my account in Gitlab.

![](images/img3.png)

Enter my Gitlab username and E-mail :

![](images/img4.png)

Now bash mode is active in my device:

![](images/img5.png)

The next step is Generating the “ssh” key pair in order to insure the connection between the Gitlab and my laptop secure, also I will not need to enter my username and password each time.

By typing this command ```ssh-keygen –t –b 4096 –c example@hotmail.com ``` as shown in the screen shot key fingerprint generate for me.

![](images/img6.png)

Then **.ssh** folder will generate, inside it there is file called **id_rsa.pud** we open it with text reader and copy all text.

Afterward, I **open Gitlab** > **settings** > **ssh keys**, I did paste the key there and press **add key**.

![](images/img7.png)

![](images/img8.png)

The last step I did receive confirmation E-mail that the key is added to my account.

![](images/img9.png)

Now I will start to create a new project:

![](images/img10.png)

We did learn two important commands that we will use it always which are:

1. Cloning a Gitlab Project: Transfer the files and folders from Gitlab to my device (locally) and the used command is:

    ```  git clone git@gitlab.com:Fatma5_/fatma_website.git
    ```

The link used in the command above we can get it from Gitlab as shown in the screenshot bellow:

![](images/img11.png)

When we do clone in Mac, it massage will appear asking “ Are you sure you want to continue connecting (yes/No)? “  we must answer yes.

2. Pushing to GitLab : Put folders and files in Gitlab and the used commands are:

```
git add –all

git commit –m “ any message”

git push
```

Whenever I do any update locally, I need to do above commands to update my project in Gitlab.

![](images/img12.png)

It important to remember before do clone or push we must select the directory.

### Access the Fab Academy 2020 Archive in Gitlab:

First we did receive E-mail form Fab Academy Coordination. Then I did Follow the log-in instruction on the E-mail to join the archive.The picture bellow shows my main project.

![](images/img77.png)

Afterward, I did Add the SSH key generated from before we I did create project in Gitlab for practice as shown bellow the SSH key is added.

![](images/img8798.png)

Next, git clone my fatima.alhashmi project.

```
git@gitlab.fabcloud.org:academany/fabacademy/2020/labs/uae/students/fatima-alhashmi.git
 ```
Afterward, I did remove MKdocs from fatima.alhashmi project  using these commands:
```
git rm -r file_name
git commit -m “message”
git push

```
However, before deleting I did put MkDocs in back up the folder in my laptop to try it or if I need to use it in future, The reason of removing the files, I had already created a website template, so no need to have the files in my repository, Also to not consume more bytes.

[MkDocs](https://www.mkdocs.org/) is a simple and static site generator that's prepared for building project documentation.

To try using MKdocs already uploaded for us in the project.We need first to have python installed and I'm already having it. So I open the terminal and did the bellow step to activate MKdocs and try it.

![](images/img798.png)


To start editing using MKdocs I type ```sudo mkdocs serve```, then IP I will copy that appeared then I pasted in Google Chrome the ready made website will open , then I open the .md files in atom and start editing any change I made will show direct in the website after saving file no need to use HTML, all the editing we be only in markdown language, after finishing editing I press (ctrl +c ) to shutting down MKdocs serve.

![](images/img2343.png)

Here I did summarize the commands for activation MKdocs:

```
python --version

sudo easy_install pip

pip --version

sudo pip install mkdocs

sudo pip install mkdocs-material

mckdocs –version

Sudo mckdocs serve
```


#### Creating a Website

For the website I used free templet from [colorlib](https://colorlib.com/), this was my first time to creating website using HTML ( Hypertext Markup Language ).Its used for documents designed to display in a web browser. I did use [Brackets](http://brackets.io/) which is text editor to do the needed changes in the templets for my website, I like using Brackets because it allowing having both screen of the website and live preview together to work in code and see the changes happens in website in same time. In addition, I did use [Atom](https://atom.io/) its editor that create md files using Markdown language. This language similar to HTML but more easy and understandable, I select to use it in written my documentation to save time as I don’t have experience in HTML. After I finish the .md file I convert it to HTML using [Pandoc](https://pandoc.org/), I do convert it because I need to re-put in brackets which only dealing with HTML.

Screen shot of my work in Brackets :

![](images/img13.png)

The figure bellow shows Pandoc command that convert .md file to HTML file.

![](images/img14.png)

Moreover, to have the converted HTML file in css style I want in need to add it in the Pandoc command as shown:


![](images/fccs.png)



 Also I have to add the css file in folder where I there is the .md and HTML files, I did download this css style form [here](). I was really annoyed how the HTML file come out after converting, Many Thanks for my colleague [Maha](http://fabacademy.org/2020/labs/uae/students/meha-hashmi/) <3. She did helped me and shows me the steps of downloading and set css style.

Screen shot of my work in Atom:

![](images/img155.png)

#### Accessing my website in Gitlab:

First I did copy( CI code for plain HTML websites )from this [link](https://about.gitlab.com/blog/2016/04/07/gitlab-pages-setup/), Then I go to the project I want to create link for it and press **set up CI/CD**and paste the copied code their and click **commit changes** , afterward, a link will created in pages all the steps shown in figure bellow.

![](images/img16.png)

The above steps I did it when I was trying Gitlab in pre Fab Academy, but for Accessing the Fab Academy Archive I found the link direct when I go to in the project page **settings** >> **pages** no need to set up CI/CD.

#### Uploading files to archive


## Problems I faced :

1. First problem that I cant push my website files in gitlab because I deleted some of old files in folder locally without update my gitlab, I fixed this problem by typing git pull command.

 ![](images/img17.png)

2. My second problem was I did push all files related to website in one folder in gitlab ,so no thing was appearing, I fixed this problem by take out all files from inside the folder and re-push it.

3. My third problem was I name the choose to .md file contain ( # ) and gitlab don’t understand so the link was not working, I fixed this problem by changing the name of the file.
