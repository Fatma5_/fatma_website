# Week 5: Embedded Programming

## **Week objectives:**

### Individual Task:
Read a microcontroller data sheet.
Program your board to do something. with as many different programming languages and programming environments as possible.

### Group Task:
Compare the performance and development workflows for other architectures.

### Used software:
- Arduino 1.8.10

For this week we did use the Hello echo board we did build in Electronic Desgin week to Program it.

The aim of the Program to let the LED when the button is pressed.

First we did had some recall of definitions  :
To start learning in this week, they Instructor ask from as to download Code::Blocks and Freematics software was only available in Windows so as i am Mac user we did use laptops from the lab to practice.

[Code::Blocks](https://en.wikipedia.org/wiki/Code::Blocks) : is a free, open source cross-platform IDE support several compilers such as C,C++, and Fortran.


[Freematics](https://sourceforge.net/projects/freematics/) : Brings freedom to telematics projects involving OBD-II, GPS, MEMS sensor and wireless technologies based on open-source hardware.

[AVR GCC ToolChains](https://www.microchip.com/mplab/avr-support/avr-and-arm-toolchains-c-compilers) is a collection of tools/libraries used to create applications for AVR microcontrollers. This collection includes compiler, assembler, linker and Standard C and math libraries.



[Let us first understand how a program, using C compiler, is executed on a host machine :](https://www.tutorialspoint.com/compiler_design/compiler_design_overview.htm)

- Human write (High level language) understandable such as C,C++.
- Compiler  --> Generate assembly code (low level language).
- Assembler --> Converts the assembly language to (machine-level language - zeros and ones-).
- Linker    --> Tool used to link all the parts of the program together for execution (executable machine code).
- Loader    -->loads all of them into memory and then the program is executed


[Makefile](https://www.includehelp.com/c-programming-questions/what-is-makefile.aspx) : is a tool to organize code for compilation.

[For the input / output Ports and Pins we used three important register:](https://pilinux.me/avr-c-programming/hardware-registers/ddrx/)

1. DDRx  : Set input / output pins.

2. PORTx : Set value for output pins.

3. PINx  : Reading input values.

Programming ATMEGA328P microcontroller with Arduino IDE:

We need :

1. Laptop with these software:

- Code :: blocks

- Freematics

-

2. Arduino.

3. الوصلة

4. Data sheet of the microcontroller in used Arduino, in my case it was Atmega328p.





To Program it we did use more than one language which are :
 - C language.
 - Arduino language.



Code using C language:

```
#include <avr/io.h>

int main(void) {


DDRB |= (1<<PB2);  //SET OUTPUT LED
DDRA &= ~(1<<PA7); // SET INPUT BOTTON
PORTA |= (1<<PA7);



while(1)

      {
if(PINA &(1<<PA7))
{
PORTB &=~ (1<<PB2);
}
else
{
PORTB |= (1<<PB2);

      }
      }
}
```
I used my board schematic and Data sheet to know where to connect the input and output, in my case the input is the button its was connected in port A , pin 7 (PA7), on the other hand the output LED was connected in PB2.

First i did declare the output and output using this commands :

```DDRB |= (1<<PB2)
```
```DDRA &= ~(1<<PA7)
```

The below command read the value of the input wither its ON/OFF :

``` PORTA |= (1<<PA7)
 ```

Then i did use while loop with always true condition :


Code using Arduino language:

```

 int const LED = 8; // the pin on my bored not in AtTiny 44
  int const BOTTON = 7;
  int botton_stat = 0;



void setup() {
  // put your setup code here, to run once:
pinMode(LED,OUTPUT);
pinMode(BOTTON,INPUT);
digitalWrite(BOTTON,HIGH);

}
void loop() {

botton_stat = digitalRead(BOTTON);

if (botton_stat == HIGH)
{
    digitalWrite(LED,LOW);   // turn the LED off by making the voltage LOW
}
else{
  digitalWrite(LED,HIGH);   // turn the LED on (HIGH is the voltage level)


}

}
```



<div class="sketchfab-embed-wrapper">
   <iframe title="A 3D model" width="640" height="480" src="https://sketchfab.com/models/794127c2a95c4a629fa0846d7acfc671/embed" frameborder="0" allow="autoplay; fullscreen; vr" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

<p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;">
   <a href="https://sketchfab.com/3d-models/oddish-794127c2a95c4a629fa0846d7acfc671?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Oddish</a>
   by <a href="https://sketchfab.com/hayden8or?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Hayden VanEarden</a>
   on <a href="https://sketchfab.com?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Sketchfab</a>
</p>
</div>
